package com.domain.models.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_product")
public class Product implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //sistem akan otomatis mengganti nama menjadi katapertama_katakedua.
    @Column(name="NamaProduk",length=200)
    private String nama;

    private String deskripsi;

    private Double harga;
    
    
    public Product() {
    }


    public Product(Long id, String nama, String deskripsi, Double harga) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getNama() {
        return nama;
    }


    public void setNama(String nama) {
        this.nama = nama;
    }


    public String getDeskripsi() {
        return deskripsi;
    }


    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }


    public Double getHarga() {
        return harga;
    }


    public void setHarga(Double harga) {
        this.harga = harga;
    }


    

    
}
