package com.domain.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.models.entities.Product;
import com.domain.models.repos.ProductRepo;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepo productRepo;

    //Create and Update
    public Product save(Product product){
        return productRepo.save(product);
    }

    //Read one record
    public Product findOne(Long id){
        Optional<Product> product = productRepo.findById(id);
        if(!product.isPresent()){
         return null;
        } 
        return product.get();
        
    }

    //Read all record
    public Iterable<Product> findAll() {
        return productRepo.findAll();
    }

    //Delete one record
    public void removeOne(Long id){
        productRepo.deleteById(id);
    }

    //Fungsi Kustom
    public List<Product> findByName(String nama) {
        return productRepo.findByNamaContains(nama);
    }
    

}
